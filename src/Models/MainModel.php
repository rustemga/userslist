<?php

namespace Users\Models;

use Users\Core\Model,
Users\Storages\FileData;

class MainModel extends Model
{
    private FileData $data;

    public function __construct()
    {
        $this->data = new FileData(getenv('USERS_FILE'));
    }

    public function getData(): array
    {
        return $this->data->getData();
    }
}
