<?php

namespace Users\Models;

use Users\Core\Model,
Users\Storages\FileData;

class DeleteModel extends Model
{
    private FileData $dataUsers;

    public function __construct()
    {
        $this->dataUsers = new FileData(getenv('USERS_FILE'));
    }

    public function getData($data = null)
    {
        $this->dataUsers->deleteUser($data["id"]);
    }
}
