<?php

namespace Users\Models;

use Users\Core\Model,
    Users\Storages\FileData,
    Users\Objects\User;
use Users\Helpers\Validator;

class UpdateModel extends Model
{
    private FileData $dataUsers;
    private Validator $validator;

    public function __construct()
    {
        $this->dataUsers = new FileData(getenv('USERS_FILE'));
        $this->validator = new Validator($_POST["name"], $_POST["username"], $_POST["email"], $_POST["phone"], $_POST["website"]);
    }

    public function updateData($data = null)
    {
        $error = $this->validator->validate();

        if (!empty($error)) {
            return $error;
        } else {
            $this->dataUsers->updateUser($_POST, $data["id"]);
            return $this->getData($data);
        }
    }

    public function getData($data = null): ?User
    {
        return $this->dataUsers->getUser($data["id"]);
    }
}
