<?php

namespace Users\Models;

use Users\Core\Model,
Users\Storages\FileData,
Users\Objects\User;

class ViewModel extends Model
{

    private FileData $dataUsers;

    public function __construct()
    {
        $this->dataUsers = new FileData(getenv('USERS_FILE'));
    }

    public function getData($data = null): ?User
    {
        return $this->dataUsers->getUser($data["id"]);
    }
}
