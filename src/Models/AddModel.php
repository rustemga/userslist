<?php

namespace Users\Models;

use Users\Core\Model,
    Users\Storages\FileData,
    Users\Objects\User,
    Users\Helpers\Validator;

class AddModel extends Model
{
    private FileData $data;
    private Validator $validator;

    public function __construct()
    {
        $this->data = new FileData(getenv('USERS_FILE'));
        $this->validator = new Validator($_POST["name"], $_POST["username"], $_POST["email"], $_POST["phone"], $_POST["website"]);
    }

    public function getData()
    {
        $error = $this->validator->validate();

        if(!empty($error)){
            return $error;
        }else{
            return $this->data->addUser($_POST);
        }
    }
}
