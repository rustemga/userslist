<?php

namespace Users\Helpers;

use Users\Objects\User,
    Users\Objects\Error;

/**
 * Validator
 * 
 * Class that validating data from inputs
 */
class Validator
{
    private User $user;
    private Error $error;
    private bool $valid = true;

    public function __construct(string $name, string $username, string $email, string $phone, string $website)
    {
        $this->user = new User($id = "", $name, $username, $email, $phone, $website);
        $this->error = new Error($this->user);
    }

    private function validateName(): void
    {
        if (empty($this->user->getName())) {
            $this->valid = false;
            $this->error->addError("name", "Name is requaired.");
        } elseif (strlen($this->user->getName()) < 3) {
            $this->valid = false;
            $this->error->addError("name", "Your name must be more then 2 characters length.");
        }
    }

    private function validateUsername(): void
    {
        if (empty($this->user->getUsername())) {
            $this->valid = false;
            $this->error->addError("username", "Username is requaired.");
        } elseif (strlen($this->user->getUsername()) < 3) {
            $this->valid = false;
            $this->error->addError("username", "Your username must be more then 2 characters length.");
        }
    }

    private function validateEmail(): void
    {
        if (empty($this->user->getEmail())) {
            $this->valid = false;
            $this->error->addError("email", "Email is requaired.");
        } elseif (!filter_var($this->user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $this->valid = false;
            $this->error->addError("email", "The Email address must be valid.");
        }
    }

    public function validate()
    {
        $this->validateName();
        $this->validateUsername();
        $this->validateEmail();

        if(!$this->valid){
            return $this->error;
        }
    }
}
