<?php
if (file_exists(dirname(__DIR__, 2) . '/vendor/autoload.php')) :
    require_once dirname(__DIR__, 2) . '/vendor/autoload.php';
else :
    echo 'Something went wrong!';
endif;

use Users\Core\Router,
Dotenv\Dotenv;

$dotenv = Dotenv::createUnsafeImmutable(dirname(__DIR__, 2));
$dotenv->load();

$router = new Router;
$router->start();