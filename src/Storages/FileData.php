<?php

namespace Users\Storages;

use Users\Objects\User;

/**
 * FileData
 * 
 * Class that retrive data from data base and working with it
 */
class FileData
{
    private string $file;

    public function __construct($file)
    {
        $this->file = $file;
    }
    
    /**
     * Method getData
     * 
     * Read data from data base
     *
     * @return array
     */
    public function getData(): array
    {
        return json_decode(file_get_contents($this->file), true);
    }
    
    /**
     * Method getUser
     *
     * @param string $id The ID of user that instans of User class must be create
     *
     * @return User
     */
    public function getUser(string $id): ?User
    {
        $users = $this->getData();
        foreach ($users as $user) :
            if ($user["id"] == $id) :
                return new User($user["id"], $user["name"], $user["username"], $user["email"], $user["phone"], $user["website"]);
            endif;
        endforeach;

        return null;
    }
    
    /**
     * Method updateUser
     * 
     * Update User
     *
     * @param array $data data from http POST request
     * @param string $id ID of updated user
     *
     * @return void
     */
    public function updateUser(array $data, string $id)
    {
        $users = $this->getData();
        foreach ($users as $i => $user) :
            if ($user["id"] == $id) :
                $users[$i] = array_merge($user, $data);
            endif;
        endforeach;

        file_put_contents($this->file, json_encode($users));
    }
    
    /**
     * Method addUser
     * 
     * Crete new user in data base
     *
     * @param array $data data from http POST request
     *
     * @return User
     */
    public function addUser(array $data): User
    {
        foreach ($data as $i => $var) :
            if (empty($var)) :
                $data[$i] = "";
            endif;
        endforeach;

        $users = $this->getData();
        $usersCount = count($users);
        $data["id"] =$users[$usersCount-1]["id"] + 1;
        array_push($users, $data);

        file_put_contents($this->file, json_encode($users));

        $user = new User($data["id"], $data["name"], $data["username"], $data["email"], $data["phone"], $data["website"]);
        
        return $user;

    }
    
    
    /**
     * Method deleteUser
     * 
     * Delete user
     *
     * @param string $id ID of user that must be deleted
     *
     * @return void
     */
    public function deleteUser(string $id)
    {
        $users = $this->getData();
        foreach ($users as $i => $user) :
            if ($user["id"] == $id) :
                array_splice($users, $i, 1);
                var_dump($users[$i]);
            endif;
        endforeach;

        file_put_contents($this->file, json_encode($users));
    }
}
