<?php

namespace Users\Core;

abstract class Model
{
     abstract public function getData();
}
