<?php

namespace Users\Core;

use FastRoute;

class Router
{
    private $dispatcher;

    private function setDispatcher(): void
    {
        $this->dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
            $r->addRoute('GET', '/', 'MainController');
            $r->addRoute('GET', '/view/{id:\d+}', 'ViewController');
            $r->addRoute('GET', '/update/{id:\d+}', 'UpdateViewController');
            $r->addRoute('POST', '/update/{id:\d+}', 'UpdateController');
            $r->addRoute('GET', '/add', 'AddViewController');
            $r->addRoute('POST', '/add', 'AddController');
            $r->addRoute('GET', '/delete/{id:\d+}', 'DeleteController');


        });
    }

    private function checkRequest()
    {
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $this->uri = rawurldecode($uri);

        return $this->dispatcher->dispatch($httpMethod, $uri);
    }

    private function checkController()
    {
        $routeInfo = $this->checkRequest();

        switch ($routeInfo[0]) {
            // Если нет страницы
            case FastRoute\Dispatcher::NOT_FOUND:
                // ... 404 Не найдена страница
                return "404";
                break;
            // Если нет метода для обработки
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                // ... 405 Нет метода
                return "405";
                break;
            // Если всё нашлось
            case FastRoute\Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                $handler = "Users\\Controllers\\". $handler;
                $controller = new $handler;
                $controller->actionIndex($vars);
                break;
        }
    }

    public function start()
    {
        $this->setDispatcher();
        return $this->checkController();
    }
}
