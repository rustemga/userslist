<div class="card">
    <div class="card-header">
        <h2>View User: <?php echo $data->getName(); ?></h2>
    </div>
    <div class="card-body">
        <table class="table">
            <tbody>
                <tr>
                    <th>Id:</th>
                    <td><?php echo $data->getId(); ?></td>
                </tr>
                <tr>
                    <th>Name:</th>
                    <td><?php echo $data->getName(); ?></td>
                </tr>
                <tr>
                    <th>Username:</th>
                    <td><?php echo $data->getUsername(); ?></td>
                </tr>
                <tr>
                    <th>Email:</th>
                    <td><?php echo $data->getEmail(); ?></td>
                </tr>
                <tr>
                    <th>Phone:</th>
                    <td><?php echo $data->getPhone(); ?></td>
                </tr>
                <tr>
                    <th>Website:</th>
                    <td><?php echo $data->getWebsite(); ?></td>
                </tr>
            </tbody>
        </table>
        <a href="/" class="btn btn-info">Users List</a>
        <a href="/update/<?php echo $data->getId(); ?>" class="btn btn-outline-secondary">Update</a>
        <a href="/delete/<?php echo $data->getId(); ?>" class="btn btn-outline-danger">Delete</a>
    </div>
</div>