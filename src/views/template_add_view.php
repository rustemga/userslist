<?php
$error;
$user;
if (isset($data)) {
    $error = $data->getErrors();
    $user = $data->getUser();
}
?>
<div class="card">
    <div class="card-header">
        <h2>Add New User</h2>
    </div>
    <div class="card-body">
        <form method="POST" enctype="multipart/form-data">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Name:</th>
                        <td>
                            <div class="form-group">
                                <input name="name" type="text" class="form-control <?php echo $error['name'] ? 'is-invalid' : ''; ?>" value="<?php echo $user ? $user->getName() : '';  ?>">
                                <div class="invalid-feedback">
                                    <?php echo $error ? $error['name'] : ''; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Username:</th>
                        <td>
                            <div class="form-group">
                                <input name="username" type="text" class="form-control <?php echo $error['username'] ? 'is-invalid' : ''; ?>" value="<?php echo $user ? $user->getUsername() : '';  ?>">
                                <div class="invalid-feedback">
                                    <?php echo $error ? $error['username'] : ''; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <td>
                            <div class="form-group">
                                <input name="email" type="text" class="form-control <?php echo $error['email'] ? 'is-invalid' : ''; ?>" value="<?php echo $user ? $user->getEmail() : '';  ?>">
                                <div class="invalid-feedback">
                                    <?php echo $error ? $error['email'] : ''; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td><input name="phone" type="tel" class="form-control" value="<?php echo $user ? $user->getPhone() : '';  ?>"></td>
                    </tr>
                    <tr>
                        <th>Website:</th>
                        <td><input name="website" type="text" class="form-control" value="<?php echo $user ? $user->getWebsite() : '';  ?>"></td>
                    </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-success">Add</button>
            <a href="/" class="btn btn-info">Users List</a>
        </form>
    </div>
</div>