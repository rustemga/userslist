<div class="card">
    <div class="card-header">
        <h2>All Users</h2>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Website</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $user_data) : ?>
                    <tr>
                        <td><?php echo $user_data["id"]; ?></td>
                        <td><?php echo $user_data["name"]; ?></td>
                        <td><?php echo $user_data["username"]; ?></td>
                        <td><?php echo $user_data["email"]; ?></td>
                        <td><?php echo $user_data["phone"]; ?></td>
                        <td><?php echo $user_data["website"]; ?></td>
                        <td>
                            <a href="view/<?php echo $user_data["id"]; ?>" class="btn btn-outline-info">View</a>
                            <a href="update/<?php echo $user_data["id"]; ?>" class="btn btn-outline-secondary">Update</a>
                            <a href="delete/<?php echo $user_data["id"]; ?>" class="btn btn-outline-danger">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <a href="add" class="btn btn-outline-success">Add New User</a>
    </div>
</div>