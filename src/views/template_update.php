<?php

use Users\Objects\Error;

$error;
$user;

if ($data instanceof Error && isset($data)) {
    $error = $data->getErrors();
    $user = $data->getUser();
}

?>
<div class="card">
    <div class="card-header">
        <h2>Update User: <i><?php echo $user ? $user->getName() : $data->getName(); ?></i></h2>
    </div>
    <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data">
            <table class="table">
                <tbody>
                    <tr>
                        <th>Name*:</th>
                        <td>
                            <div class="form-group">
                                <input name="name" type="text" value="<?php echo $user ? $user->getName() : $data->getName(); ?>" class="form-control <?php echo $error['name'] ? 'is-invalid' : ''; ?>">
                                <div class="invalid-feedback">
                                    <?php echo $error ? $error['name'] : ''; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Username*:</th>
                        <td>
                            <div class="form-group">
                                <input name="username" type="text" value="<?php echo $user ? $user->getUsername() : $data->getUsername(); ?>" class="form-control <?php echo $error['username'] ? 'is-invalid' : ''; ?>">
                                <div class="invalid-feedback">
                                    <?php echo $error ? $error['username'] : ''; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Email*:</th>
                        <td>
                            <div class="form-group">
                                <input name="email" type="text" value="<?php echo $user ? $user->getEmail() :  $data->getEmail(); ?>" class="form-control <?php echo $error['email'] ? 'is-invalid' : ''; ?>">
                                <div class="invalid-feedback">
                                    <?php echo $error ? $error['email'] : ''; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Phone:</th>
                        <td>
                            <div class="form-group">
                                <input name="phone" type="tel" value="<?php echo $user ? $user->getPhone() : $data->getPhone(); ?>" class="form-control">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Website:</th>
                        <td>
                            <div class="form-group">
                                <input name="website" type="text" value="<?php echo $user ? $user->getWebsite() : $data->getWebsite(); ?>" class="form-control">
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-success">Update</button>
            <a href="/" class="btn btn-info">Users List</a>
        </form>
    </div>
</div>