<?php

namespace Users\Objects;

/**
 * Error
 */
class Error
{
    private array $errors;
    private User $user;
    
    /**
     * Method __construct
     *
     * @param User $user
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * Method addError
     *
     * @param string $key
     * @param string $errorMessage
     *
     * @return void
     */
    public function addError(string $key, string $errorMessage)
    {
        $this->errors[$key] = $errorMessage;
    }
    
    /**
     * Method getErrors
     *
     * @return Array
     */
    public function getErrors(): Array
    {
        return $this->errors;
    }
    
    /**
     * Method getUser
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}