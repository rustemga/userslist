<?php

namespace Users\Objects;

class User
{

    private string $id;
    private string $name;
    private string $username;
    private string $email;
    private string $phone;
    private string $website;

    public function __construct(string $id, string $name, string $username, string $email, string $phone, string $website)
    {
        $this->id = $id;
        $this->name = $name;
        $this->username = $username;
        $this->email = $email;
        $this->phone = $phone;
        $this->website = $website;
    }

    public function getId(): ?String
    {       
        return $this->id;
    }

    public function getName(): ?String
    {       
        return $this->name;
    }

    public function getUsername(): ?String
    {       
        return $this->username;
    }

    public function getEmail(): ?String
    {       
        return $this->email;
    }

    public function getPhone(): ?String
    {       
        return $this->phone;
    }

    public function getWebsite(): ?String
    {       
        return $this->website;
    }
}
