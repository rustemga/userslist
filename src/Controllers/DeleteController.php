<?php

namespace Users\Controllers;

use Users\Core\Controller,
    Users\Core\View,
    Users\Models\DeleteModel;

class DeleteController
{
    public $model;
    public $view;

    public function __construct()
    {
        $this->model = new DeleteModel();
        $this->view = new View();
    }

    public function redirect($url, $permanent = false)
    {
        header('Location: ' . $url, true, $permanent ? 301 : 302);

        exit();
    }
    
    public function actionIndex($UriData = null)
    {
        $data = $this->model->getData($UriData);

        //$this->view->generate('template_main.php', 'template_view.php', $data);

        $this->redirect("/", false);
    }
}
