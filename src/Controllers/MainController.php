<?php

namespace Users\Controllers;

use Users\Core\Controller,
Users\Core\View,
Users\Models\MainModel;

class MainController extends Controller
{

    public function __construct()
    {
        $this->model = new MainModel();
		$this->view = new View();
    }

    public function actionIndex($data = null) 
	{	
        $data = $this->model->getData();
		$this->view->generate('template_main.php', 'template_view.php', $data);
        
	}
}