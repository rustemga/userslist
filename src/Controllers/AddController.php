<?php

namespace Users\Controllers;

use Users\Core\Controller,
    Users\Core\View,
    Users\Models\AddModel,
    Users\Objects\User;

class AddController extends Controller
{
    public $model;
    public $view;

    public function __construct()
    {
        $this->model = new AddModel();
        $this->view = new View();
    }

    public function actionIndex($UriData = null)
    {
        $data = $this->model->getData();

        if ($data instanceof User) {
            $this->view->generate('template_user_view.php', 'template_view.php', $data);
        } else {
            $this->view->generate('template_add_view.php', 'template_view.php', $data);
        }
    }
}
