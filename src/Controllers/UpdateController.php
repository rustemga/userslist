<?php

namespace Users\Controllers;

use Users\Core\Controller,
Users\Core\View,
Users\Models\UpdateModel;

class UpdateController extends Controller
{
    public function __construct()
    {
        $this->model = new UpdateModel();
        $this->view = new View();
    }

    public function actionIndex($UriData = null)
    {
        $data = $this->model->updateData($UriData);

        if ($data == null) {
            $this->view->generate('template_not_user_view.php', 'template_view.php', $UriData["id"]);
        } else {
            $this->view->generate('template_update.php', 'template_view.php', $data);
        }
    }
}