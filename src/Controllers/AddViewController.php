<?php

namespace Users\Controllers;

use Users\Core\Controller;

class AddViewController extends Controller
{
    public function actionIndex($UriData = null)
    {
        $this->view->generate('template_add_view.php', 'template_view.php');
    }
}
