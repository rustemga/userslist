<?php

namespace Users\Controllers;

use Users\Models\ViewModel,
    Users\Core\View;

class ViewController
{
    public function __construct()
    {
        $this->model = new ViewModel();
        $this->view = new View();
    }

    public function actionIndex($UriData = null)
    {
        $data = $this->model->getData($UriData);

        if ($data == null) {
            $this->view->generate('template_not_user_view.php', 'template_view.php', $UriData["id"]);
        } else {
            $this->view->generate('template_user_view.php', 'template_view.php', $data);
        }
    }
}
